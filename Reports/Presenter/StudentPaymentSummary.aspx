﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentPaymentSummary.aspx.cs" Inherits="Abundance_Nk.Web.Reports.Presenter.StudentPaymentSummary" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
          <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeOut="60000">
         </asp:ScriptManager>
    <div>
    
        <asp:Button ID="Button1" runat="server" Height="33px" OnClick="Button1_Click" Text="Show Report" Width="125px" />
    
    </div>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="463px" style="margin-top: 112px" Width="934px">
        </rsweb:ReportViewer>
    </form>
</body>
</html>
